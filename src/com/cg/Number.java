package com.cg;

import static com.cg.MayanCalculation.digits;

class Number {
        Digit value;
        Number prev;

        Number(Digit value) {
            this.value = value;
        }

        Number(long number) {
            value = digits[(int) (number % 20)];
            number /= 20;
            if (number > 0) {
                Number tmp = prev;
                prev = new Number(number);
                prev.prev = tmp;
            }
        }

        @Override
        public String toString() {
            if (prev == null) {
                return value.toString();
            }
            return prev + "" + value;
        }

        String toMaya() {
            if (prev == null) {
                return value.digit;
            }
            return prev.toMaya() + "\n" + value.digit;
        }

        Long getValue() {
            long v = 0;
            Number n = this.reverse();
            while (n != null) {
                v += n.value.getValue();
                n = n.prev;
                if (n != null)
                    v *= 20;
            }
            return v;
        }

        Number reverse() {
            Number res = null;
            Number n = this;
            while (n != null) {
                Number tmp = res;
                res = new Number(n.value);
                res.prev = tmp;
                n = n.prev;
            }
            return res;
        }

        Number plus(Number n) {
            Number prev = this;
            Number nPrev = n;
            int remain = 0;
            int sum = 0;
            Number res = null;
            while (prev != null || nPrev != null) {
                if (prev != null) {
                    sum += prev.value.getValue();
                    prev = prev.prev;
                }
                if (nPrev != null) {
                    sum += nPrev.value.getValue();
                    nPrev = nPrev.prev;
                }
                sum += remain;
                remain = sum / 20;
                Number tmp = res;
                res = new Number(digits[sum % 20]);
                res.prev = tmp;
                sum = 0;
            }
            if (remain > 0) {
                Number tmp = res;
                res = new Number(digits[remain]);
                res.prev = tmp;
            }
            return res.reverse();
        }

        Number minus(Number n) {
            Number prev = this;
            Number nPrev = n;
            int remain = 0;
            int sum = 0;
            Number res = null;
            while (prev != null || nPrev != null) {
                if (prev != null) {
                    sum += prev.value.getValue();
                    prev = prev.prev;
                }
                if (nPrev != null) {
                    sum -= nPrev.value.getValue();
                    nPrev = nPrev.prev;
                }
                sum -= remain;
                if (sum < 0) {
                    sum += 20;
                    remain = 1;
                } else {
                    remain = 0;
                }
                Number tmp = res;
                res = new Number(digits[sum % 20]);
                res.prev = tmp;
                sum = 0;
            }
            while (res != null && res.value.getValue() == 0) {
                res = res.prev;
            }
            if (res == null) {
                res = new Number(digits[0]);
            }
            return res.reverse();
        }

        Number time(Digit digit) {
            Number prev = this;
            Number res = null;
            int remain = 0;
            int prod;
            while (prev != null) {
                prod = prev.value.getValue() * digit.getValue();
                prod += remain;
                remain = prod / 20;
                Number tmp = res;
                res = new Number(digits[prod % 20]);
                res.prev = tmp;
                prev = prev.prev;
            }
            if (remain > 0) {
                Number tmp = res;
                res = new Number(digits[remain]);
                res.prev = tmp;
            }
            return res.reverse();
        }

        Number time20() {
            Number n = this;
            Number tmp = n;
            n = new Number(digits[0]);
            n.prev = tmp;
            return n;
        }

        Number time(Number n) {
            Number res = new Number(digits[0]);
            int i = 0;
            while (n != null) {
                Number part = this.time(n.value);
                for (int j = 0; j < i; j++) {
                    part = part.time20();
                }
                res = res.plus(part);
                i++;
                n = n.prev;
            }
            Number reverse = res.reverse();
            while (reverse != null && reverse.value.getValue() == 0) {
                reverse = reverse.prev;
            }
            if (reverse == null) {
                reverse = new Number(digits[0]);
            }
            return reverse.reverse();
        }

        Number divide(Number n) {
            Number res = new Number(getValue() / n.getValue());
            return res;
        }

    }