package com.cg;

import static com.cg.MayanCalculation.digitsChar;
import static com.cg.MayanCalculation.digitsMap;

class Digit {
        String digit;

        Digit() {
        }

        void append(String partial) {
            if (digit == null) {
                digit = partial;
            } else {
                digit += "\n" + partial;
            }
        }

        Integer getValue() {
            return digitsMap.get(digit);
        }

        @Override
        public String toString() {
            return digitsChar[getValue()] + "";
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Digit digit1 = (Digit) o;

            return digit.equals(digit1.digit);

        }

        @Override
        public int hashCode() {
            return digit.hashCode();
        }
    }