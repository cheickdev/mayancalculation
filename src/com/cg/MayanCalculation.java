package com.cg;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by sissoko on 15/04/2016.
 */
public class MayanCalculation {

    static Map<String, Integer> digitsMap = new HashMap<>();
    static Digit[] digits = new Digit[20];
    static char[] digitsChar = new char[20];

    static {
        for (int i = 0; i < 20; i++) {
            digits[i] = new Digit();
            digitsChar[i] = (char) ('A' + i);
        }
    }

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int L = in.nextInt();
        int H = in.nextInt();
        for (int i = 0; i < H; i++) {
            String numeral = in.next();
            for (int j = 0; j < numeral.length(); j += L) {
                String partial = numeral.substring(j, j + L);
                digits[j / L].append(partial);
            }
        }

        for (int i = 0; i < 20; i++) {
            digitsMap.put(digits[i].digit, i);
        }

        long S1 = in.nextInt();
        Number first = null, second = null;
        long HW = S1 / H;
        for (int i = 0; i < HW; i++) {
            Digit digit = new Digit();
            for (int j = 0; j < H; j++) {
                String num1Line = in.next();
                digit.append(num1Line);
            }
            Number prev = first;
            first = new Number(digits[digitsMap.get(digit.digit)]);
            first.prev = prev;
        }
        int S2 = in.nextInt();
        HW = S2 / H;
        for (int i = 0; i < HW; i++) {
            Digit digit = new Digit();
            for (int j = 0; j < H; j++) {
                String num2Line = in.next();
                digit.append(num2Line);
            }
            if (second == null) {
                second = new Number(digits[digitsMap.get(digit.digit)]);
            } else {
                Number prev = second;
                second = new Number(digits[digitsMap.get(digit.digit)]);
                second.prev = prev;
            }
        }
        String operation = in.next();
        // Write an action using System.out.println()
        // To debug: System.out.println("Debug messages...");
        if (operation.equals("+")) {
            System.out.println(first.plus(second).toMaya());
        }
        if (operation.equals("-")) {
            System.out.println(first.minus(second).toMaya());
        }
        if (operation.equals("*")) {
            System.out.println(first.time(second).toMaya());
        }
        if (operation.equals("/")) {
            System.out.println(first.divide(second).toMaya());
        }
    }
}
